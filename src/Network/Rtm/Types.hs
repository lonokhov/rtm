{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Network.Rtm.Types
    ( ConnectionId (..)
    , SubscriptionId (..)
    , QuitReason (..)
    ) where

import           Data.Aeson (FromJSON, ToJSON)
import           Data.Hashable (Hashable)

-- | Id for a client connection.
newtype ConnectionId = ConnectionId Int
  deriving (Show, Eq, Ord, FromJSON, ToJSON, Hashable)

-- | Id of subscription of one client. Used for event notification
--  and unsbuscribing
newtype SubscriptionId = SubscriptionId Int
  deriving (Show, Eq, Ord, FromJSON, ToJSON, Hashable)

-- | Reason for client to quit connection
data QuitReason = InternalReason
                | QueueOverflowReason
                | ClosedReason -- A generic connection-closed reason
                | ServerShutdownReason
                deriving (Eq, Ord, Show)

