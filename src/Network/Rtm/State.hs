{-# LANGUAGE BangPatterns     #-}
{-# LANGUAGE FlexibleContexts #-}
-- | 'Rtm' state tracks connections and subscriptions.
module Network.Rtm.State
    ( SendQueue
    , Connection (..)
    , Subscription (..)
    , ConnectionStatus (..)
      -- * Rtm state object
    , Rtm (..)
    , newRtm
    , startRtm
    , stopRtm
      -- * Connection management
    , registerConnection
    , unregisterConnection
    , connectionStatus
    , setDisconnectedStatus
      -- * Managing subscriptions
    , addSubscription
    , removeSubscription
    , removeClientSubscriptions
    , broadcastEvent
      -- * Sending messages to client
    , sendMessage
    , nextQueuedMessage
    ) where

import           Control.Concurrent.STM (STM, atomically, check, orElse)
import           Control.Concurrent.STM.TBQueue
    (TBQueue, isFullTBQueue, newTBQueueIO, readTBQueue, writeTBQueue)
import           Control.Concurrent.STM.TVar
    (TVar, modifyTVar', newTVarIO, readTVar, readTVarIO, writeTVar)

import           Data.Foldable (for_, traverse_)
import           Data.Hashable
import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import           Data.IORef (IORef, atomicModifyIORef', newIORef)
import           Numeric.Natural (Natural)

import           Network.Rtm.Types
    (ConnectionId (..), QuitReason (..), SubscriptionId (..))

-- | Message queue for client messages
type SendQueue msg = TBQueue msg

sMaxQueueSize :: Natural
sMaxQueueSize = 100

-- | Connection status tracks if client should be disconnect.
data ConnectionStatus = Connected
                      | Disconnected QuitReason
                      deriving (Eq, Ord, Show)

-- | A one way to-client connection
data Connection k msg = Connection
  { conId                 :: !ConnectionId
  , conSendQueue          :: !(SendQueue msg)
  , conStatus             :: !(TVar ConnectionStatus)
  , conSubscriptions      :: !(TVar (HashMap SubscriptionId k))
  , conNextSubscriptionId :: !(IORef Int)
  }

grabSubscriptionId :: Connection k m -> IO SubscriptionId
grabSubscriptionId con =
  atomicModifyIORef' (conNextSubscriptionId con) $ \i ->
     (succ i, SubscriptionId i)



-- | A subscription of one client to one key.
-- Client can have multiple subscriptions on same key.
data Subscription m = Subscription
    { subscriptionQueue :: !(SendQueue m)
    , subscriptionIds   :: ![SubscriptionId]
    }

dropSubscription :: SubscriptionId -> Subscription m -> Maybe (Subscription m)
dropSubscription sid (Subscription q sids) =
    case filter (/= sid) sids of
      sids' | null sids' -> Nothing
            | otherwise  -> Just (Subscription q sids')


type ConnectionMap k m = HashMap ConnectionId (Connection k m)

type SubscriptionMap k m = HashMap k (HashMap ConnectionId (Subscription m))

-- | Tracks state of 'Rtm' system, connections and subscriptions.
data Rtm k m = Rtm
  { rtmRunning          :: TVar Bool
  , rtmConnections      :: !(TVar (ConnectionMap k m))
  , rtmNextConnectionId :: !(IORef Int)
  , rtmSubscriptions    :: !(TVar (SubscriptionMap k m))
  }

-- | Create new empty 'Rtm'
newRtm :: (Eq k, Hashable k) => IO (Rtm k m)
newRtm = Rtm <$> newTVarIO False
             <*> newTVarIO mempty
             <*> newIORef 0
             <*> newTVarIO mempty

-- | Only execute STM if server is running
withRunning :: Rtm k m -> b -> STM b -> STM b
withRunning st e act = do
   r <- readTVar (rtmRunning st)
   (check r *> act) `orElse` pure e

-- | Only execute STM if server is running with Either result
withRunningE :: Rtm k m -> a -> STM b -> STM (Either a b)
withRunningE st e act = do
   r <- readTVar (rtmRunning st)
   (check r *> (Right <$> act)) `orElse` pure (Left e)

startRtm :: (Eq k, Hashable k) => Rtm k m -> IO ()
startRtm st = do
  connections <- atomically $ do
        writeTVar (rtmRunning st) True
        writeTVar (rtmSubscriptions st) mempty
        readTVar  (rtmConnections st)
  -- Kill connections just in case
  for_ connections $ flip setDisconnectedStatus ServerShutdownReason

stopRtm :: (Eq k, Hashable k) => Rtm k m -> IO ()
stopRtm st = do
  connections <-atomically . withRunning st mempty $ do
     writeTVar (rtmRunning st) False
     writeTVar (rtmSubscriptions st) mempty
     cons <- readTVar (rtmConnections st)
     writeTVar (rtmConnections st) mempty
     pure cons
  for_ connections $ flip setDisconnectedStatus ServerShutdownReason


grabConnectionId :: Rtm k m -> IO ConnectionId
grabConnectionId st =
  atomicModifyIORef' (rtmNextConnectionId st) $ \i ->
      (succ i, ConnectionId i)

-- | Register new connection in 'Rtm'
registerConnection :: Rtm k m -> IO (Either QuitReason (Connection k m))
registerConnection st = do
  cid <- grabConnectionId st
  con <- Connection cid
         <$> newTBQueueIO sMaxQueueSize
         <*> newTVarIO Connected
         <*> newTVarIO mempty
         <*> newIORef 0
  atomically . withRunningE st ServerShutdownReason $ do
     modifyTVar' (rtmConnections st) $ HashMap.insert cid con
     pure con

-- | Gets a connections status.
connectionStatus :: Connection k m -> IO ConnectionStatus
connectionStatus con = readTVarIO (conStatus con)

-- | Marks connection as closed and sets reason unless connection
-- is already closed
setDisconnectedStatus :: Connection k m -> QuitReason -> IO ()
setDisconnectedStatus con q = atomically $ do
  st <- readTVar (conStatus con)
  case st of
    Connected -> writeTVar (conStatus con) $ Disconnected q
    _ -> pure ()


-- | Unregister connection from 'Rtm' and sets @Disconnected ClosedReason@
-- status on 'Connection'. Also removes subscriptions
unregisterConnection :: (Eq k, Hashable k)
                     => Rtm k m
                     -> Connection k m
                     -> IO ()
unregisterConnection st con = do
  setDisconnectedStatus con ClosedReason
  removeConnection st con
  removeClientSubscriptions st con

-- | Remove connection from 'Rtm'
removeConnection :: Rtm k m -> Connection k m -> IO ()
removeConnection st con =
  atomically . withRunning st () $ do
     connMap <- readTVar (rtmConnections st)
     for_ (HashMap.lookup (conId con) connMap) $ \_ ->
        writeTVar (rtmConnections st) $! HashMap.delete (conId con) connMap

-- | Adds subscription to state so it would receive updates on key
addSubscription :: (Eq k, Hashable k)
                => Rtm k m
                -> Connection k m
                -> k
                -> IO (Maybe SubscriptionId)
addSubscription st con key = do
  sid <- grabSubscriptionId con
  atomically . withRunning st Nothing $ do
    -- If not running then should soon stop anyway, so don't report
    status <- readTVar (conStatus con)
    if status /= Connected
     then pure Nothing -- Do not subscribe dead connections
     else do
       modifyTVar' (conSubscriptions con) $ HashMap.insert sid key
       modifyTVar' (rtmSubscriptions st) $ \h ->
        let upd = case HashMap.lookup key h of
              Nothing -> newSubscriptionMap sid
              Just subs ->
                  case HashMap.lookup (conId con) subs of
                    Nothing -> addNewSubscription sid subs
                    Just sub -> updateSubscription sid sub subs
         in HashMap.insert key upd h
       pure $ Just sid
  where
    newSubscription sid = Subscription (conSendQueue con) [sid]
    newSubscriptionMap sid = HashMap.singleton (conId con) $ newSubscription sid
    addNewSubscription sid = HashMap.insert (conId con) $ newSubscription sid
    updateSubscription sid (Subscription q sids) =
        HashMap.insert (conId con) $ Subscription q (sid:sids)

-- | Remove subscription from state so it stops receiveing updates
removeSubscription :: (Eq k, Hashable k)
                   => Rtm k m
                   -> Connection k m
                   -> SubscriptionId -> IO ()
removeSubscription st con sid =
  atomically . withRunning st () $ do
    clientSubs <- readTVar (conSubscriptions con)
    for_ (HashMap.lookup sid clientSubs) $ \key ->
        modifyTVar' (rtmSubscriptions st) $ deleteOrDrop dropFromChannel key
  where
    dropFromChannel = HashMap.update (dropSubscription sid) (conId con)

-- | Remove all subscriptions of given client
removeClientSubscriptions :: (Eq k, Hashable k)
                          => Rtm k m
                          -> Connection k m
                          -> IO ()
removeClientSubscriptions st con =
  atomically . withRunning st () $ do
     clientConns <- readTVar (conSubscriptions con)
     modifyTVar' (rtmSubscriptions st) $ \allSubs ->
       let
          dropSub = deleteOrDrop (HashMap.delete (conId con))
        in HashMap.foldr dropSub allSubs clientConns

-- | If inner hashmap is empty after update then it will be deleted it
deleteOrDrop :: (Eq k, Hashable k)
             => (HashMap l v -> HashMap l v)
             -> k
             -> HashMap k (HashMap l v)
             -> HashMap k (HashMap l v)
deleteOrDrop f = HashMap.update doit
  where
   doit v = case f v of
              v' | HashMap.null v' -> Nothing
                 | otherwise -> Just v'
{-# INLINE deleteOrDrop #-}

-- | Send message to all subscribers
broadcastEvent :: (Eq k, Hashable k)
               => Rtm k msg
               -> k
               -> ([SubscriptionId] -> msg)
               -> IO ()
broadcastEvent st key mkMsg = do
   subscriptions <- HashMap.lookup key <$> readTVarIO (rtmSubscriptions st)
   flip (traverse_ . traverse_) subscriptions $ \s ->
       queueMessage (subscriptionQueue s) $
         mkMsg (subscriptionIds s)
   pure()

{-# INLINE broadcastEvent #-}

queueMessage :: SendQueue msg -> msg -> IO ()
queueMessage q !msg = atomically $
   writeTBQueue q msg `orElse` pure ()
{-# INLINE queueMessage #-}

-- | Write message to client queue, unless it is full. If full then discard it.
sendMessage :: Connection k msg -> msg -> IO ()
sendMessage con !msg = queueMessage (conSendQueue con) msg

{-# INLINE sendMessage #-}

--nextQueuedMessage :: Connection k -> Either QuitReason
-- | Deque next message from send queue. Blocks if queue is empty.
-- If Connection status is 'Disconnected' then returns the reason.
-- If queue is full then chanages connection status to
-- @Disconnected QueueOverflowReason@.
nextQueuedMessage :: Connection k msg -> IO (Either QuitReason msg)
nextQueuedMessage con = atomically $ do
  quit <- readTVar (conStatus con)
  case quit of
    Disconnected r -> pure (Left r)
    Connected -> do
      isFull <- isFullTBQueue (conSendQueue con)
      if isFull
         then do
           writeTVar (conStatus con) $ Disconnected QueueOverflowReason
           pure (Left QueueOverflowReason)
         else
           Right <$> readTBQueue (conSendQueue con)
