{-# LANGUAGE FunctionalDependencies #-}
-- | Typeclass for working with 'Rtm' state
module Network.Rtm.Class
    ( HasRtm (..)
    , newConnection
    , dropConnection
    , subscribe
    , unsubscribe
    , broadcast
    ) where

import           Control.Monad.IO.Class (MonadIO (..))
import           Data.Hashable (Hashable)

import           Network.Rtm.State
    (Connection (..), Rtm (..), addSubscription, broadcastEvent,
    registerConnection, removeSubscription, unregisterConnection)
import           Network.Rtm.Types (QuitReason, SubscriptionId)

-- | Class abstracting access to 'Rtm' state.
class HasRtm k msg m | m -> k, m -> msg where
  askRtm :: m (Rtm k msg)


-- | Create new connection and register it in 'Rtm' state.
newConnection
  :: HasRtm k msg m
  => MonadIO m
  => m (Either QuitReason (Connection k msg))
newConnection = do
  rtm <- askRtm
  liftIO $ registerConnection rtm

-- | Remove connection from 'Rtm' state, including subscriptions.
dropConnection :: ( Eq k
                  , Hashable k
                  , HasRtm k msg m
                  , MonadIO m )
               => Connection k msg
               -> m ()
dropConnection con = do
  rtm <- askRtm
  liftIO $ unregisterConnection rtm con

-- | Subsribe to a topic
subscribe :: ( Eq k, Hashable k
             , HasRtm k msg m
             , MonadIO m )
            => Connection k msg
            -> k
            -> m (Maybe SubscriptionId)
subscribe con key = do
  rtm <- askRtm
  liftIO $ addSubscription rtm con key

-- | Remove subscription
unsubscribe :: ( Eq k, Hashable k
               , HasRtm k msg m
               , MonadIO m )
            => Connection k msg
            -> SubscriptionId
            -> m ()
unsubscribe con sid = do
  rtm <- askRtm
  liftIO $ removeSubscription rtm con sid

broadcast
  :: Eq k
  => Hashable k
  => HasRtm k msg m
  => MonadIO m
  => k
  -> ([SubscriptionId] -> msg)
  -> m ()
broadcast k msg = do
  rtm <- askRtm
  liftIO $ broadcastEvent rtm k msg

